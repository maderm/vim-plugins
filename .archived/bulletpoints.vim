" bulletpoints.vim:     Transforms lines into markdown bulletpoints
" Author:               Mathias Mader [mathias.mader@math.ethz.ch]
" Version:              1.2

if exists('g:bulletpoint_loaded')
  finish
endif
let g:bulletpoint_loaded = 1

let s:hasBullet = '^\*\s'
let s:bullet = "\* "
let s:bulletRegex = '^\s\{2\}\*\s'

function s:HasBullet(lineNumber)
    return getline(a:lineNumber) =~ s:hasBullet
endfunction

function s:AddBullet(lineNumber)
    let line = getline(a:lineNumber)
    let startOfText = match(line, '\S')

    let lineText = []
    for i in range(0, (startOfText - 1))
        call add(lineText, line[i])
    endfor
    call setline(a:lineNumber, $'{join(lineText)}' .. $'{substitute(trim(line), "^", s:bullet, "")}')
endfunction

function s:RemoveBullet(lineNumber)
    call setline(a:lineNumber, $'{substitute(getline(a:lineNumber), s:bulletRegex, "", "")}')
endfunction

function ToggleBullet()
    let lineNumber = line('.')
    if s:HasBullet(lineNumber)
        call s:RemoveBullet(lineNumber)
    else
        call s:AddBullet(lineNumber)
    endif
endfunction

" vim: set ts=4 sw=4 syntax=vim ai et: 
