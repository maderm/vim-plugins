" todo.vim      Adds and markdown checkboxes and toggles their state
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      1.0

if exists('g:todo_loaded')
  finish
endif
let g:todo_loaded = 1

let s:hasCheckBox = '-\ \[\.\]\ '
let s:isChecked = '-\ \[\S\]\ '
let s:checked = '-\ \[x\]\ '
let s:unchecked = '-\ \[\ \]\ '

function s:IsChecked(line)
    return a:line =~ s:isChecked
endfunction

function s:HasCheckBox(line)
   return a:line =~ s:hasCheckBox
endfunction

function AddToDo()
    let lineNumber = line('.')
    let line = getline(lineNumber)
    if s:HasCheckBox(line)
        return
    else
        let fswIndex = match(line, '\S')
        let chars = []

        for i in range(0, (fswIndex - 1))
            call add(chars, line[i])
        endfor

        call setline(lineNumber, $'{join(chars)}' .. $'{substitute(trim(line), "^", s:unchecked, "")}')
    endif
endfunction

function ToggleCheck()
    let lineNumber = line('.')
    let line = getline(lineNumber)
    if s:IsChecked(line)
        return setline(lineNumber, substitute(line, s:checked, s:unchecked, ""))
    else
        return setline(lineNumber, substitute(line, s:unchecked, s:checked, ""))
    endif
endfunction

" vim: set ts=4 sw=4 syntax=vim ai et: 
