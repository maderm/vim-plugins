" custom_rc_file_loader.vim - Loads an additional vimrc file "                             Depending on weither the env vars SSH_CLIENT or
"                             WHOISROOT is set.
"
" Author:                     Mathias Mader [mathias.mader@math.ethz.ch]
" Version:                    1.0 initial version
"
" Date:                       30. Juli 2024

function LoadCustomConfig(custom_vimrc_file)
    if filereadable($"{a:custom_vimrc_file}")
        execute("source" .. $"{a:custom_vimrc_file}")
        echom "custom vimrc file " .. $"{a:custom_vimrc_file}" .. " loaded."
    endif
endfunction

function s:GetCustomConfig()
    if has_key(environ(), 'SSH_CLIENT')
        echom "Environment variable SSH_CLIENT is set."
        let custom_rc_name = split(getenv('SSH_CLIENT'))[0]
        LoadCustomConfig(expand(resolve($'/root/.vimrc_{custom_rc_name}')))
    elseif has_key(environ(), 'WHOISROOT')
        echom "Environment variable WHOISROOT is set."
        let custom_rc_name = getenv('WHOISROOT')
        LoadCustomConfig(expand(resolve($'/root/.vimrc_{custom_rc_name}')))
    endif
endfunction

if exists('g:custom_rc_file_plug_loaded')
    finish
endif

let g:custom_rc_file_plug_loaded = 1

call LoadCustomConfig()

" vi:ts=4:sw=4:syntax=vim:et:ai
