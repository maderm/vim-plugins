" load_maderms_vimrc.vim   Loads my super awesome vimrc.
" Author:              Mathias Mader [mathias.mader@math.ethz.ch]
" Version:             0.1 initial untested version
"

if exists('g:load_maderms_vimrc')
  finish
endif
let g:load_maderms_vimrc = 1

function LoadMadermsConfig()
    let s:settings = '/root/maderm/server-dotfiles/vim/.vimrc'
    if filereadable($"{s:settings}")
        execute("source" .. $"{s:settings}")
        echom "custom vimrc file " .. $"{a:custom_vimrc_file}" .. " loaded."
    else
        echom "[ERROR] custom vimrc file " .. $"{a:custom_vimrc_file}" .. " not found!"
    endif
endfunction
