function Load_whoisroot_rc()
    let s:root_account = expand($WHOISROOT)
    if strlen(s:root_account) > 0
        let s:custom_vimrc = expand($HOME) . '/' . '.vimrc_' . s:root_account
        if getftype(s:custom_vimrc) == 'link'
            let s:realpath = string(resolve(s:custom_vimrc))
            source s:Eval_rc_file(s:realpath)
        else
            source s:Eval_rc_file(s:custom_vimrc)
        endif
    endif
endfunction

function s:Eval_rc_file(path)
    source a:path
    for line in readfile(a:path)
        eval line
    endfor
endfunction

if v:vim_did_enter
    call Load_whoisroot_rc()
else
    au VimEnter * call Load_whoisroot_rc()
endif

" vim: set ts=4 sw=4 syntax=vim ai et: 
