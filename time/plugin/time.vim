" time.vim - Some handy functions to insert time and dates into documents.
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      1.5: Re-added plugin load protection.
"               1.4: Added insert mode mappings.
"               1.3: 'p' instead of 'P'
"                    Now it's only mappings
"               1.1: Added mappings
"               1.0

if exists('g:time_tools_loaded')
    finish
endif
let g:time_tools_loaded = 1

nmap <Leader>da "=strftime("%d. %B %Y")<CR>P<ESC>
nmap <Leader>ti "=strftime("%H:%M")<CR>p<ESC>

imap <M-d> <ESC><Leader>da<ESC>a
imap <M-t> <ESC><Leader>ti<ESC>a

" vi:ts=4:sw=4:syntax=vim:softtabstop=4:ai
