" lunchlogin.vim - Inserts the time that I've logged in after my lunch break.
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      1.0: initial release
 
if exists('g:lunchlogin_ext_loaded')
  finish
endif
let g:lunchlogin_ext_loaded = 1

function GetLunchLoginTime()
    term_getline()
    execute ":r!doas journalctl -ea | grep -E 'gdm-session-worker.+tty.+success' | head -n1 | grep -o -E '[[:digit:]]+:[[:digit:]]+'<CR>"
endfunction
