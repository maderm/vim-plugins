" codeblock.vim:        Surrounds text with a markdown code block.
" Author:               Mathias Mader [mathias.mader@math.ethz.ch]
" Version:              0.1

if exists('g:codeblock_loaded')
  finish
endif
let g:codeblock_loaded = 1

fun Codeblock(range)

endfun


" vim: set ts=4 sw=4 syntax=vim ai et: 
