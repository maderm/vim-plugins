" ShBoxes.vim          To make fancy cli messages.
" Author:              Mathias Mader [mathias.mader@math.ethz.ch]
" Version:             1.0
" :redi[r] @{a-zA-Z}>	Redirect messages to register {a-z}.  Append to the
" 			contents of the register if its name is given
" 			uppercase {A-Z}.  The ">" after the register name is
" 			optional.

" source: https://stackoverflow.com/a/6271254
function! s:get_visual_selection()
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)
    if len(lines) == 0
        return ''
    endif
    let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]
    return join(lines, "\n")
endfunction

function Box()
	let vsel = s:get_visual_selection()
    execute ":echo vsel"
endfunction

" tempname()					*tempname()* *temp-file-name*
" 		The result is a String, which is the name of a file that
" 		doesn't exist.  It can be used for a temporary file.  The name
" 		is different for at least 26 consecutive calls.  Example: >
" 			:let tmpfile = tempname()
" 			:exe "redir > " .. tmpfile
"  		For Unix, the file will be in a private directory |tempfile|.
" 		For MS-Windows forward slashes are used when the 'shellslash'
" 		option is set, or when 'shellcmdflag' starts with '-' and
" 		'shell' does not contain powershell or pwsh.
" 


" vim: set ts=4 sw=4 syntax=vim ai et: 
