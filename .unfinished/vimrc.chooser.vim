if v:version >= 900 && has('vim9script')
    echo 'vim9script detected!'
    if has_key(environ(), 'WHOISROOT')
        let username = environ()["WHOISROOT"]
        if username == "maderm"
            source $HOME/.vim/vimrc_maderm
        endif
    endif
else
    echo 'old school vim detected!'
    if exists($WHOISROOT)
            let username = expand($WHOISROOT)
            if exists($HOME/.vim/vimrc_maderm)
                    source $HOME/.vim/vimrc_maderm
            endif
    endif
endif
