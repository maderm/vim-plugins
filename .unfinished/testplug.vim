" testplug.vim: just a file to test some stuff
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      0.1
" Date:         Tue Apr 18 2023

" if exists('g:testplug_loaded')
"   finish
" endif
" let g:testplug_loaded = 1

" nmap <leader>cb :call AddCodeBlock()<CR>
vnoremap <leader>cb :<c-u>call AddCodeBlock(visualmode())<cr>
nnoremap <leader>cb :set operatorfunc=AddCodeBlock<cr>g@

let s:firstLine = 0
let s:lastLine = 0
let s:isRangeSet = 0
let s:codeBlock = "```"

function s:SetCols(firstCol, lastCol)
    let s:firstCol = a:firstCol
    let s:lastCol = a:lastCol
endfun

function s:SetRange(firstLine, lastLine)
    let s:firstLine = a:firstLine
    let s:lastLine = a:lastLine
    echom $'start set to {s:firstLine}, end set to {s:lastLine}'
    let s:isRangeSet = 1
endfunction

function AddCodeBlock(type)
    if a:type ==# 'v'
        call s:SetCols(line('v'), line('.'))
    elseif a:type ==# 'V'
        call s:SetRange(line("'<"), line("'>"))
    elseif a:type ==# 'char'
        call s:SetRange(line("."), line("."))
    elseif a:type ==# 'line'
        call s:SetRange(line("."), line("."))
    else
        call s:SetRange(line("."), line("."))
    endif

    if s:isRangeSet == 0
        echom "ERROR: No range set!"
        return
    endif

    if s:firstLine == s:lastLine
        execute $'normal! :{s:firstLine},{s:lastLine}delete x\<CR\>' 
        call setline(s:firstLine, $"`{getreg('x')}`")
    else
        call setreg('x', getline(s:firstLine, s:lastLine))
        call setline(s:firstLine, "`" .. getreg('x') .. "`")

        "       execute $':{s:firstLine},{s:lastLine}delete x'
        "       execute "normal O```\<Esc>"
        "       execute "normal O```\<Esc>"
        "       execute 'normal "xp'
    endif
endfunction

" vim: set ts=4 syntax=vim
