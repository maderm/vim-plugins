" getlistfromcmd.vim -  Adds a modeline at the last line with the current settings.
" Author:               Mathias Mader [mathias.mader@math.ethz.ch]
" Version:              0.1 initial version.
"
" Changelog:            -
"
" Date:                 05. September 2024

if exists('g:getlistfromcmd_loaded')
    finish
endif
"
let g:getlistfromcmd_loaded = 1

" function s:GetQfItem(line)
"     return { 'filename': a:line, 'text': a:line}
" endfunction

function! GetListFromCmd(cmd)
    if len(a:cmd) == 0
        return
    endif

    call setqflist([], ' ', {'nr' : '$', 'lines' : systemlist(a:cmd)})
    :copen
endfunction
