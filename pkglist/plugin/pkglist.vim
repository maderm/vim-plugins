" vi:ts=4:sw=4:sts=4:syntax=vim:fdm=syntax:ai:et
" pkgs.vim      Adds dnf related functions for comfort when viewing pkg
"               files.
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      0.1 initial version
" Date:         05. November 2024

" Macros to use.
" yE:!dnf info "
" yE:!dnf repoquery -l "
" Y:!doas dnf install "kb

function DoSearch()
    normal mm
    normal 0
    normal 'm
    let s:pkg_name = expand('<cWORD>')
    if empty(s:pkg_name) == 1
        echom "No word found to interpret as package name :("
    else
        let s:cmd = "!dnf search " .. $"{s:pkg_name}"
        execute $"{s:cmd}"
    endif
endfunction

function GetInfo()
    normal mm
    normal 0
    normal 'm
    let s:pkg_name = expand('<cWORD>')
    if empty(s:pkg_name) == 1
        echom "No word found to interpret as package name :("
    else
        let s:cmd = "!dnf info " .. $"{s:pkg_name}"
        execute $"{s:cmd}"
    endif
endfunction

function GetFileList()
    normal mm
    normal 0
    normal 'm
    let s:pkg_name = expand('<cWORD>')
    if empty(s:pkg_name) == 1
        echom "No word found to interpret as package name :("
    else
        let s:cmd = "!dnf repoquery -l " .. $"{s:pkg_name}"
        execute $"{s:cmd}"
    endif
endfunction

function Install()
    normal mm
    normal 0
    normal 'm
    let s:pkg_name = expand('<cWORD>')
    if empty(s:pkg_name) == 1
        echom "No word found to interpret as package name :("
    else
        let s:cmd = "!doas dnf --disableexcludes=all install " .. $"{s:pkg_name}"
        execute $"{s:cmd}"
    endif
endfunction

