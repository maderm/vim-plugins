" pkgs.vim      Adds dnf related functions for comfort when viewing pkg
"               files.
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      0.1 initial version
" Date:         05. November 2024

let g:debug_pkgs_plugin = 1

" Macros to use.
" yE:!dnf info "
" yE:!dnf repoquery -l "
" Y:!doas dnf install "kb

func GetInfo()
    " Example line:
    " package-name.x86_64 [lots of whitespace] [version] [lots of white space] [repo] [lots of white space]
    " Concrete examples:
    " zynthian-data-padthv1.noarch                                                             1.0.0.9cccef5-3.fc38                                             copr:copr.fedorainfracloud.org:ycollet:audinux
    " zytrax.x86_64                                                                            0.9.0.97b79d1-3.fc38                                             copr:copr.fedorainfracloud.org:ycollet:audinux
    "
    " Therefore:
    " Grab first WORD of the line
    " Fetch information  about it
    "
    if g:debug_pkgs_plugin == 1
        let s:line_number = line('.')
        echo "Line number is:\t" .. $"{s:line_number}"
    endif

    normal 0
    let s:name = expand('<cWORD>')
    if s:name =~? '\S\+'
        echo "The first word of the line is:\t" .. $"{s:name}"
    else
        echo "The line is empty."
    endif
endfunc


" vi:ts=4:sw=4:sts=4:syntax=vim:fdm=syntax:ai:et
