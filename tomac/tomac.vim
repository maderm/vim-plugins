" tomac.vim     Converts a bunch of (12 to be exact) characters to a mac adress
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      1.0 - Initial Version - 12. August 2024
"
function! ToMacAdress()
    execute ':s/\v(\S{2})(\S{2})(\S{2})(\S{2})(\S{2})(\S{2})(\s*)/\U\1:\2:\3:\4:\5:\6\7/g'
endfunction

if exists('g:tomac_ext_loaded')
  finish
endif
let g:tomac_ext_loaded = 1

" nnoremap <Leader> tm :call ToMacAdress()<CR>
" vnoremap <Leader> tm :call ToMacAdress()<CR>

" vi:ts=4:sw=4:syntax=vim:et:ai
