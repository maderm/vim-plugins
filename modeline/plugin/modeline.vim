" modeline.vim - Adds a modeline at the last line with the current settings.
"
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      1.81
"
" Changelog:    1.81: fix bug that prevented the modeline to
"               actually be added.
"               1.7: added additional trim
"               1.6: no notes
"               1.0: no notes
"
" Date:         09. September 2024

if exists('g:ml_loaded')
    finish
endif

let g:ml_loaded = 1
let s:settings = ''
let s:isModeLine = '^\S\svim:\|^\S\svi:'

function s:GetFoldMethod()
    if len(&filetype) == 0
        return 'fdm=manual'
    elseif &filetype ==? 'text'
        return 'fdm=manual'
    else
        return 'fdm=syntax'
    endif
endfunction

function s:GetSyntax()
    if len(&filetype) == 0
        return ''
    else
        return 'syntax=' . $'{&filetype}'
    endif
endfunction

function s:GetModelineString(setting)
    return len(a:setting > 0) ? $'{a:setting}' : ''
endfunction

function s:IsKnownComment()
    if &filetype ==? 'vim'
        return 1
    elseif &filetype ==? 'markdown'
        return 1
    elseif &filetype ==? 'sh'
        return 1
    elseif &filetype ==? 'bash'
        return 1
    elseif &filetype ==? 'zsh'
        return 1
    elseif &filetype ==? 'gitconfig'
        return 1
    elseif &filetype ==? 'javascript'
        return 1
    elseif &filetype ==? 'python'
        return 1
    else
        return 0
    endif
endfunction

function s:GetComment()
    if &filetype ==? 'vim'
        return '" '
    elseif &filetype ==? 'markdown'
        return ' '
    elseif &filetype ==? 'sh'
        return '# '
    elseif &filetype ==? 'bash'
        return '# '
    elseif &filetype ==? 'zsh'
        return '# '
    elseif &filetype ==? 'gitconfig'
        return '# '
    elseif &filetype ==? 'javascript'
        return '// '
    elseif &filetype ==? 'python'
        return '# '
    else
        return ' '
    endif
endfunction

function s:GetSettings()
    let s:ts = 'ts=' . $'{&ts}'
    let s:sw = 'sw=' . $'{&sw}'
    let s:sts = $'{&et == 1 ? $'sts={&sw}' : ''}'
    let s:syn = $'{s:GetSyntax()}'
    let s:fdm = $'{s:GetFoldMethod()}'
    let s:ai = $'{&ai == 1 ? 'ai' : ''}'
    let s:et = $'{&et == 1 ? 'et' : ''}'
    return trim($'{s:GetModelineString(s:ts)}:{s:GetModelineString(s:sw)}:{s:GetModelineString(s:sts)}:{s:GetModelineString(s:syn)}:{s:GetModelineString(s:fdm)}:{s:GetModelineString(s:ai)}:{s:GetModelineString(s:et)}')
endfunction

function s:IsModeLine(lineNumber)
    return getline(a:lineNumber) =~ s:isModeLine
endfunction

function AddModeLine()
    if s:IsModeLine(line('$'))
        echo 'Modeline is already set.'
        return
    else
        let s:settings=s:GetSettings()
        echo 'settings is: ' . $'{s:settings}'
        if s:IsKnownComment()
            let s:modeLine=$'{s:GetComment()}vi:{s:settings}'
            call append(line('$'), $'{s:modeLine}')
        else
            echom $'no comment for filetype {&filetype}'
        endif
    endif
endfunction

" vi:ts=4:sw=4:sts=4:syntax=vim:fdm=syntax:ai:et
