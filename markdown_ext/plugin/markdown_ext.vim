" markdown_ext.vim     Adds various GitLab Wiki related functions.
" Author:              Mathias Mader [mathias.mader@math.ethz.ch]
" Version:             1.1 changed function names
"                      1.0
"

if exists('g:markdown_ext_loaded')
  finish
endif
let g:markdown_ext_loaded = 1

function! PathToWiki()
    execute ":%s/\.md)/)/g"
endfunction

function! WikiToPath()
    execute ":%s/\(\/\S\+\))/\1\.md)/g"
endfunction

function AddToDo()
    let lineNumber = line('.')
    let line = getline(lineNumber)
    if s:HasCheckBox(line)
        return
    else
        let fswIndex = match(line, '\S')
        let chars = []

        for i in range(0, (fswIndex - 1))
            call add(chars, line[i])
        endfor

        call setline(lineNumber, $'{join(chars)}' .. $'{substitute(trim(line), "^", s:unchecked, "")}')
    endif
endfunction

function ToggleCheck()
    let lineNumber = line('.')
    let line = getline(lineNumber)
    if s:IsChecked(line)
        return setline(lineNumber, substitute(line, s:checked, s:unchecked, ""))
    else
        return setline(lineNumber, substitute(line, s:unchecked, s:checked, ""))
    endif
endfunction

function ToggleBullet()
    let lineNumber = line('.')
    if s:HasBullet(lineNumber)
        call s:RemoveBullet(lineNumber)
    else
        call s:AddBullet(lineNumber)
    endif
endfunction

let s:hasCheckBox = '-\ \[\.\]\ '
let s:isChecked = '-\ \[\S\]\ '
let s:checked = '-\ \[x\]\ '
let s:unchecked = '-\ \[\ \]\ '

function s:IsChecked(line)
    return a:line =~ s:isChecked
endfunction

function s:HasCheckBox(line)
   return a:line =~ s:hasCheckBox
endfunction
let s:hasBullet = '^\*\s'
let s:bullet = "\* "
let s:bulletRegex = '^\s\{2\}\*\s'

function s:HasBullet(lineNumber)
    return getline(a:lineNumber) =~ s:hasBullet
endfunction

function s:AddBullet(lineNumber)
    let line = getline(a:lineNumber)
    let startOfText = match(line, '\S')

    let lineText = []
    for i in range(0, (startOfText - 1))
        call add(lineText, line[i])
    endfor
    call setline(a:lineNumber, $'{join(lineText)}' .. $'{substitute(trim(line), "^", s:bullet, "")}')
endfunction

function s:RemoveBullet(lineNumber)
    call setline(a:lineNumber, $'{substitute(getline(a:lineNumber), s:bulletRegex, "", "")}')
endfunction

"
" Mappings
"
nmap <Leader>td :call AddToDo()<CR>
nmap <Leader>dd :call ToggleCheck()<CR>
nmap <Leader>ml :call AddModeLine()<CR>
" nmap <Leader>bp :call ToggleBullet()<CR>
" vmap <Leader>bp :call ToggleBullet()<CR>

" vim: set ts=4 sw=4 syntax=vim ai et:
