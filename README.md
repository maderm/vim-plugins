# vim-plugins

## What's this?

This repo is for @maderm's vim-plugins.
Uset them at your own risk.

## Installation

If not otherwise stated, just put the `.vim` file into `~/.vim/plugins` to automatically load them at vim start.
