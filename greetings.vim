" greeting.vim  Inserts greeting into an email.
" Author:       Mathias Mader [mathias.mader@math.ethz.ch]
" Version:      0.1
"
" Example:
" Gruss,
" Mathias
"
" Regards,
" Mathias

function! Greet()
    normal: iGruss,<Esc>oMathias<Esc>
endfunction

function! Gruss()
    execute ''
endfunction

if exists('g:titlecase_ext_loaded')
  finish
endif
let g:titlecase_ext_loaded = 1

nmap tc :call TitleCaseCL()<CR>
vmap tc :call TitleCaseCL()<CR>

