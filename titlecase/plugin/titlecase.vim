" titlecase.vim     Converts current line to Title Case.
" Author:           Mathias Mader [mathias.mader@math.ethz.ch]
" Version:          0.1
"
" Example: 
" bla blubb becomes Bla Blubb

function! TitleCaseCL()
    execute ':s/\<\(\w\)\(\S*\)/\u\1\L\2/g'
endfunction

if exists('g:titlecase_ext_loaded')
  finish
endif
let g:titlecase_ext_loaded = 1

nmap tc :call TitleCaseCL()<CR>
vmap tc :call TitleCaseCL()<CR>
